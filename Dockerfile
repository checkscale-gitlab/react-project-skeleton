FROM node:8.16.1-alpine as build-stage
LABEL maintainer="David Julián <david.julian@outlook.com>"
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.17.4-alpine
WORKDIR /usr/share/nginx/html
COPY --from=build-stage /app/dist/ .
const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const WebpackNotifierPlugin = require("webpack-notifier");

module.exports = {
  entry: "./src/index.tsx",
  devtool: "source-map",
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "[name].bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "ts-loader",
          },
        ],
      },
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader",
      },
    ],
  },
  plugins: [
    new WebpackNotifierPlugin({ alwaysNotify: true }),
    new HtmlWebpackPlugin({
      template: "./src/index.html",
    }),
  ],
};

# React + Typescript & Docker Boilerplate

Plantilla de aplicación React con Typescript.

## Características

- React 16.10
- Typescript 3
- Jest + Enzyme
- ESLint
- Prettier
- Webpack
- Docker & Docker Compose

## Requisitos

- Docker _17.05.0+_
- Docker Compose _1.10.0+_

## Docker

El _Dockerfile_ incluido permite crear una imagen que construye el proyecto y lo expone con un Nginx.

```bash
# Construir la imagen
docker build --rm -t react-typescript-boilerplate .

# Levantar un contenedor
docker run -p 8888:80 -d \
    --name react-ts-app \
    react-typescript-boilerplate:latest
```

Navegar a `http://localhost:8888` para acceder a la aplicación.

## Docker Compose

Durante el desarrollo es recomendable utilizar _Docker Compose_ ya que permite actualizar la aplicación con los cambios en el código.

```bash
# Levantar el ecosistema
docker-compose up -d

# Ejecutar webpack en modo *watch*
npm run start
```

Navegar a `http://localhost:8888` para acceder a la aplicación. Tras guardar cualquier cambio, refrescar la página para ver el resultado.

> El proyecto cuenta con la dependencia de desarrollo _webpack-notifier_
> gracias a la cual, aparecerá una notificación en el sistema indicando que
> webpack ha construido el proyecto.
